import java.util.Scanner;
class ApplianceStore{
	public static void main (String[] args){
		
		Scanner keyboard = new Scanner(System.in);
		Refrigerator[] aBunchOfRefrigerators = new Refrigerator[3];
		
			for(int i = 0; i < aBunchOfRefrigerators.length; i++){
				aBunchOfRefrigerators[i] = new Refrigerator();
				
				aBunchOfRefrigerators[i].coldestTemperature = keyboard.nextInt();
				
				aBunchOfRefrigerators[i].numOfCompartments = keyboard.nextInt();
				
				aBunchOfRefrigerators[i].smartFridgeOrNot = keyboard.nextBoolean();
			}
			
			System.out.println(aBunchOfRefrigerators[2].coldestTemperature);
			System.out.println(aBunchOfRefrigerators[2].numOfCompartments);
			System.out.println(aBunchOfRefrigerators[2].smartFridgeOrNot);
			
			System.out.println(aBunchOfRefrigerators[0].doesItFreeze());
			System.out.println(aBunchOfRefrigerators[0].connectWifi());
	}
}