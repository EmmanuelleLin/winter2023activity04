public class Refrigerator{
	
	public int coldestTemperature;
	public int numOfCompartments;
	public boolean smartFridgeOrNot;
	
	public boolean doesItFreeze(){
		if(coldestTemperature <= 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean connectWifi(){
		if(smartFridgeOrNot == true){
			return true;
		}
		else{
			return false;
		}
	}
}